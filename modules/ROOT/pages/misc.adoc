////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Miscellaneous Legal Packaging Topics

This page retains certain information on various legal or licensing aspects of Fedora packaging, such as certain non-license criteria for approval of certain kinds of packages or files in Fedora Linux, and certain specific interpretations of licensing issues made by Fedora.

== License of Fedora spec files

All original Fedora contributions are governed by the xref:fpca.adoc[Fedora Project Contributor Agreement (FPCA)]. Since every Fedora spec file is contributed by FPCA signers, this means that unless a spec file contains an explicit license or license notice, it is available under the terms of the MIT license.

== Game data or content
Some non-executable data or content exists that is required to make free and open source game engines functional. Such data or content usually comes with the game engine as freely distributable, but not necessarily under a free and open source license allowed by Fedora.  An example of this would be open sourced game engines, such as Doom, Heretic, and Descent. These game engines come with freely distributable shareware gamedata files.

In this case, the game data or content files can be packaged and included in Fedora, as long as the files meet the requirements to be otherwise allowed in Fedora, such as allowed-firmware or allowed-content.

== Emulators

Some emulators (applications which emulate another platform) are not permitted for inclusion in Fedora Linux. These rules will help you determine if an emulator is acceptable for Fedora.

* Emulators which depend on firmware or ROM files to function may not be included in Fedora Linux, unless the copyright holder(s) for the firmware/ROM files give clear permission for the firmware/ROM files to be distributed (either under a Fedora *allowed* license or a Fedora *allowed-firmware* license). Note: This only covers the situation where an emulator will not run at all without firmware/ROM files. For example, emulators that compile and run, but ship with no game ROMs are not covered by this rule.

* Emulators must not ship with any ROM files (e.g. games) unless those ROM files are available under a Fedora *allowed* license and have been built from source code in the Fedora buildsystem.

* Emulators must not point to any third-party sites which provide firmware or ROM files that are distributed without the clear and explicit permission of their copyright holders.

* All other Fedora licensing and packaging rules apply to emulators.

////
=== Licensing of RSA Implementations of MD5

It is common to encounter old RSA reference C implementations of the MD5 message-digest algorithm under a free software license containing an advertising requirement, similar to the better-known advertising clause in old versions of the BSD license. Such advertising clauses have generally been understood to be GPL-incompatible. The RSA license is included in the SPDX License List under the short identifier https://spdx.org/licenses/RSA-MD.html[RSA-MD]. The MD5 reference implementation also appears in https://datatracker.ietf.org/doc/html/rfc1321[RFC 1321], which states that distribution of the RFC document is "unlimited."

In 2000 RSA provided a https://www.ietf.org/ietf-ftp/ietf/IPR/RSA-MD-all [clarification] of "the status of intellectual property rights asserted by [RSA] in the MD2, MD4 and MD5 message-digest algorithms", saying "Implementations of these message-digest algorithms, including
implementations derived from the reference C code in RFC-1319, RFC-1320, and
RFC-1321, may be made, used, and sold without license from RSA for any
purpose."

Fedora traditionally interpreted this RSA statement as, in effect, superseding the RSA-MD license except to the extent that the license required "copyright attribution" (the category referred to as "Copyright only" under Fedora's pre-SPDX Callaway system).

Under the new policy of enumerating binary package licenses in the spec file License: field, if you encounter RSA code under this license, you do not have to record RSA-MD in the License: field, or any . You also do not have to have the RPM install a copy of the RSA license in /usr/share/licenses. You also do not need to copy or reference the external 2000 RSA statement, or use the old Callaway short name "Copyright only" in the spec file License: field. However, you should retain the RSA copyright and license notice as found in the upstream source code.
////
