////

Copyright Fedora Project Authors.

SPDX-License-Identifier: CC-BY-SA-4.0

////

= Fedora Project Contributor Agreement

The only purpose of the Fedora Project Contributor Agreement (FPCA) is to ensure that Fedora has default permission to use your contributions under a Fedora-allowed license (either MIT for software or CC BY-SA for content). You may contribute under any other allowed Fedora license; the FPCA simply sets a default if no license is specified.

Nothing in the FPCA takes anything away from you. You retain ownership of your contributions.

If you have any questions about the FPCA, please email legal@fedoraproject.org.

----
The Fedora Project Contributor Agreement
[Version 2021-05-04]

Goal
---------

We require that contributors to Fedora (as defined below) agree to
this Fedora Project Contributor Agreement (FPCA) to ensure that
contributions to Fedora have acceptable licensing terms.

Non-Goals
---------

The FPCA is *not* a copyright assignment agreement.

The FPCA does *not* somehow supersede the existing licensing terms
that apply to Fedora contributions.  There are two important subpoints
here.  First, the FPCA does not apply to upstream code (or other
material) that you didn't write; indeed, it would be preposterous for
it to attempt to do so.  Note the narrow way in which we have defined
capital-c "Contribution".  Second, the main provision of the FPCA
specifies that a default license will apply to code that you wrote,
but only to the extent that you have not bothered to put an explicit
license on it. Therefore, the FPCA is *not* some sort of special
permissive license granted to any party, despite the explicit choice
of a more restrictive license by you or by upstream developers.

Terms
---------

0.  Definitions.

"Acceptable License For Fedora" means a license selected from the
appropriate categorical sublist of the full list of acceptable
licenses for Fedora, currently located at
https://docs.fedoraproject.org/en-US/legal/allowed-licenses/,
as that list may be revised from time to time by the Fedora Council.
"Acceptable Licenses For Fedora" means that full list.

"CC-BY-SA" means the Creative Commons Attribution-ShareAlike 4.0
International license, as published at
<https://creativecommons.org/licenses/by-sa/4.0/legalcode>.

"Code" means (i) software code, (ii) any other functional material
whose principal purpose is to control or facilitate the building of
packages, such as an RPM spec file, (iii) font files, and (iv) other
kinds of copyrightable material that the Fedora Council has
classified as "code" rather than "content".

"Content" means any copyrightable material that is not Code, such as,
without limitation, (i) non-functional data sets, (ii) documentation,
(iii) wiki edits, (iv) music files, (v) graphic image files, (vi) help
files, and (vii) other kinds of copyrightable material that the Fedora
Council has classified as "content" rather than "code".

"Contribution" means a Work that You created, excluding any portion
that was created by someone else.  (For example, if You Submit a
package to Fedora, the spec file You write may be a Contribution, but
all the upstream code in the associated SRPM that You did not write is
not a Contribution for purposes of this FPCA.)  A Contribution
consists either of Code or of Content.

"Current Default License", with respect to a Contribution, means (i)
if the Contribution is Code, the MIT License, and (ii) if the
Contribution is Content, CC-BY-SA.

"Fedora" means the community project led by the Fedora Council
<https://docs.fedoraproject.org/en-US/council/>.

"Fedora Community" means (i) all Fedora participants, and (ii) all
persons receiving Contributions directly or indirectly from or through
Fedora.

"Licensed" means covered by explicit licensing terms that are
conspicuous and readily discernible to recipients.

"MIT License" means the license identified as "Modern Style with
sublicense" at
<https://fedoraproject.org/wiki/Licensing:MIT#Modern_Style_with_sublicense>.

"Submit" means to use some mode of digital communication (for example,
without limitation, mailing lists, bug tracking systems, and source
code version control systems administered by Fedora) to voluntarily
provide a Contribution to Fedora.

"Unlicensed" means not Licensed.

"Work" means a copyrightable work of authorship. A Work may be a
portion of a larger Work, and a Work may be a modification of or
addition to another Work.

"You" means the individual accepting this instance of the FPCA.

1. Copyright Permission Required for All Contributions.

If You are not the copyright holder of a given Contribution that You
wish to Submit to Fedora (for example, if Your employer or university
holds copyright in it), it is Your responsibility to first obtain
authorization from the copyright holder to Submit the Contribution
under the terms of this FPCA on behalf of, or otherwise with the
permission of, that copyright holder.  One form of such authorization
is for the copyright holder to place, or permit You to place, an
Acceptable License For Fedora on the Contribution.

2.  Licensed Contributions.

If Your Contribution is Licensed, Your Contribution will be governed
by the terms under which it has been licensed.

3.  Default Licensing of Unlicensed Contributions.

If You Submit an Unlicensed Contribution to Fedora, the license to the
Fedora Community for that Contribution shall be the Current Default
License.

The Fedora Council may, by public announcement, subsequently
designate an additional or alternative default license for a given
category of Contribution (a "Later Default License"). A Later Default
License shall be chosen from the appropriate categorical sublist of
Acceptable Licenses For Fedora.

Once a Later Default License has been designated, Your Unlicensed
Contribution shall also be licensed to the Fedora Community under that
Later Default License.  Such designation shall not affect the
continuing applicability of the Current Default License to Your
Contribution.

You consent to having Fedora provide reasonable notice of Your
licensing of Your Contribution under the Current Default License (and,
if applicable, a Later Default License) in a manner determined by
Fedora.

4.  Public Domain United States Government Works.

Sections 1 through 3 of this FPCA do not apply to any Contribution to
the extent that it is a work of the United States Government for which
copyright is unavailable under 17 U.S.C. 105.
 
5.  Acceptance.

You must signify Your assent to the terms of this FPCA through
specific electronic means established by Fedora (such as by
click-through acceptance means).

You may also, at Your option, and without eliminating the requirement
set forth in the preceding paragraph, send a copy of this FPCA,
bearing Your written signature indicating Your acceptance of its
terms, by email to legal@fedoraproject.org, by fax to +1 919 754 3704,
or by postal mail to:

Fedora Legal
c/o Red Hat, Inc.
100 East Davie Street
Raleigh, NC 27601
USA
----

== Frequently Asked questions

Q: What is the purpose of the FPCA? +
A: The FPCA exists for one main reason: to ensure that contributions to Fedora have acceptable licensing terms.

Q: If I agree to the FPCA, am I assigning copyright to Fedora or Red Hat? +
A: No. The FPCA is not a copyright assignment agreement.

Q: Does this mean that Fedora will always relicense my contributions from $MY_LICENSE to MIT? +
A: No. If you put a Fedora-allowed license on your contribution, we will use it under the terms of that license. If you put it under a not-allowed license, we won't use it at all. Only unlicensed contributions where the copyright holder is the Fedora contributor qualify for the "default licensing" clause.

Q: If I make a contribution to a Fedora project that is clearly licensed under GPLv2-or-later, and my changes don't have an explicit GPL or other license notice on them, does the FPCA mean I am making my changes under the MIT license?+
A: No. In this case, Fedora views your contribution as "Licensed", i.e., "covered by explicit licensing terms that are conspicuous and readily discernible to recipients". Those licensing terms are GPLv2-or-later. So your changes are licensed under GPLv2-or-later and the FPCA default license does not apply.

Q: Do I need to physically sign the FPCA? +
A: No. We require that all contributors agree to it digitally, through the https://accounts.fedoraproject.org/[Fedora Accounts System]. If you wish to additionally sign a physical copy and send it to us, the FPCA describes how to do this, but it is *not* required.

Q: Are all Fedora users/distributors required to agree to the FPCA? +
A: No. Only Fedora contributors will be required to agree to the FPCA. Although, if you want to agree to it, you can. :)

Q: The FPCA defines "default licenses" of MIT for code, and Creative Commons Attribution-ShareAlike 4.0 for content, why not $OTHER_LICENSE? +
A: The MIT license was chosen for code because of its widespread use (including in Fedora-related projects and Fedora Linux packages) and compatibility with other free licenses. CC BY-SA was chosen (originally version 3.0, later version 4.0) because it came to be significantly preferred by Fedora contributors developing documentation and content for Fedora. 

Q: If I'm granting the MIT license or CC BY-SA by default, how is Fedora dealing with the notice or attribution requirements of those licenses? +
A: The FPCA says: "You consent to having Fedora provide reasonable notice of Your
licensing of Your Contribution under the Current Default License (and,
if applicable, a Later Default License) in a manner determined by
Fedora." Since adoption of the FPCA, Fedora has been treating as "reasonable notice" public notice of the FPCA as an inbound licensing policy coupled with the public record of particular Fedora contributions; any other approach would be impractical. Fedora views this "reasonable notice of Your licensing" as covering license notice provisions of the MIT license and CC BY-SA. As for attribution under CC BY-SA, Fedora's view is that any contributor of "Unlicensed Content" does not desire attribution because they did not specify an attribution preference when making the contribution. You can always use an explicit license on your contributions if you prefer, including any preferred copyright notice or author attribution.

Q: Are RPM spec files covered by the FPCA? +
A: Yes, assuming a given spec file is subject to copyright. Spec files are explicitly named as an example of a contribution, to clear up a past confusion.

Q: Can we translate the FPCA into other languages? +
A: Yes, although only the English text is binding for the purposes of agreement. Any translations which are created are not "legal translations", and exist only to assist non-English speaking contributors. All Fedora contributors must agree to the English text.

Q: I have a question/suggestion/flame about the FPCA that is not covered here, where should I send it to? +
A: If you want to discuss it in public, please subscribe to the https://admin.fedoraproject.org/mailman/listinfo/legal[fedora-legal-list] and post your thoughts there. If you do not wish to discuss it in public, feel free to send it via email to legal@fedoraproject.org.

Q: Can I use the FPCA as a license for my code/content? +
A: Well, technically, you can do whatever you want, but you really shouldn't. It wouldn't work very well.

Q: My FOSS friendly project would like to take the FPCA, change Fedora to the name of our project, and use the FPCA, can we do this? +
A: Sure. You can consider the FPCA to be under the terms of the Creative Commons Attribution-ShareAlike 4.0, with the notable exception of the Fedora trademarks, which may only be used under the Fedora Trademark Guidelines. If modified, attribution should occur in the actual document itself. For attribution in other scenarios, please contact legal@fedoraproject.org. One piece of advice: If you decide to make changes beyond simply changing the name of the FPCA or replacing the Fedora trademarks, you really should consult with a lawyer, to make sure that the document is still legally valid and says what you mean. Remember, legalese is not English.

Q: Who wrote this amazing masterpiece of legal text? +
A: Many people were involved in helping to craft this text. The primary author was Richard Fontana, with feedback from Tom Callaway, Pamela Chestek, Paul Frields, and Robert Tiller. Feel free to give them gifts (for example, drinks or tasty snacks) as thank yous, although, this is not a requirement (legal or otherwise). ;)

== Historical FAQs

Some of the following FAQs refer to the Fedora Individual Contributor License Agreement (ICLA), which preceded adoption of the FPCA in 2011 and which was no longer used after adoption of the FPCA.

Q: Why change the Fedora ICLA to the FCPA? +
A: The previous agreement, the Fedora ICLA, wasn't really well structured for the needs of Fedora. It was composed of a lot of legal boilerplate, and was written before Fedora had really taken shape. In fact, the only reason that we were able to leverage it for as long as we did is because of some creative interpretation on the part of Fedora Legal. Also, there were many people who could not agree to the Fedora ICLA for a variety of reasons, and we hoped that the FPCA would resolve most (if not all) of those concerns.

Q: Why did you change the name from ICLA to FPCA? +
A: The new text is not really a "Contributor License Agreement" in the traditional sense. The FPCA exists for one main reason: to ensure that contributions to Fedora have acceptable licensing terms. We chose a name that did not use "CLA" to avoid confusion and to mark it as a distinctly specific license.

Q: When was the Fedora ICLA retired? +
A: There was be a window of time (between Tuesday May 17, 2011 and Friday June 17, 2011) when we attempted to get all current Fedora contributors who had agreed to the Fedora ICLA to agree to the FPCA. Once that window closed, any contributors in the system who did not agree to the FPCA were removed from "cla_done" (note: including membership in other, dependent FAS groups).

Q: What changed between the 2011-03-29 Version and the 2015-02-03 Version? +
A. The 2011-03-29 version referenced the Fedora Board, which has been obsoleted/replaced by the Fedora Council. The FPCA text was updated to reflect that change. No other changes were made.

Q: What changed between the 2015-02-03 Version and the 2021-05-04 Version? +
A: The Fedora Council updated the default content license to the Creative Commons Attribution-ShareAlike 4.0 International (CC-BY-SA-4.0) license.

Q: What changed between the 2021-05-04 Version and the 2021-10-25 edit? +
A: The Fedora Council removed accidentally-overlooked clauses in the text which were made obsolete by the update to CC-BY-SA 4.0. The FAQ was also updated to refer to the 4.0 version of the license.
