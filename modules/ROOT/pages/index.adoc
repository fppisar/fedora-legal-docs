:page-aliases: council:policy/legal.adoc

= Fedora Legal Resources

This documentation covers various Fedora policies, guidelines and other information pertaining to licensing and other legal issues.

NOTE: Much of the Fedora guidelines on licensing and how to populate the `License:` field in spec files was updated in July 2022. Please review this documentation!

== Licensing in Fedora
In order to meet the goal of creating a complete, general purpose operating system exclusively from free and open source software, all software and other content packaged in Fedora Linux, or otherwise used in or created for Fedora, must be under licenses determined to be allowed in Fedora, with only limited, conditional exceptions.

See the links here for understanding xref:license-approval.adoc[what kinds of licenses are allowed in Fedora], lists of xref:allowed-licenses.adoc[allowed] and xref:not-allowed-licenses.adoc[not-allowed] licenses, xref:license-field.adoc[populating the `License:` field] in a package spec file, and the process for xref:license-review-process.adoc[reviewing new licenses].

== Legal Mailing List

Fedora has a legal mailing list (legal@lists.fedoraproject.org). Only list subscribers can 
post directly to the list. You can subscribe to the list and view the archives at 
https://lists.fedoraproject.org/admin/lists/legal.lists.fedoraproject.org/.

== FE-Legal
There is a Bugzilla tracker bug named https://bugzilla.redhat.com/show_bug.cgi?id=182235[FE-Legal]. If a package review ticket is blocked by the FE-Legal tracker bug, this means that the person who set the block believes that the package raises an issue under Fedora's licensing guidelines or other aspects of the Fedora Project's legal policies.

Package reviews blocked by the https://bugzilla.redhat.com/show_bug.cgi?id=182235[FE-Legal] tracker bug cannot be approved until the block is officially lifted.

== Legacy Note Regarding fedora.info
Cornell University and the University of Virginia offer an open source digital object repository software under the mark FEDORA Project. Red Hat's Fedora Project is not affiliated, connected or associated with the FEDORA Project of Cornell University and the University of Virginia. Cornell University and the University of Virginia do not sponsor, approve of, or endorse Red Hat's Fedora Project.

